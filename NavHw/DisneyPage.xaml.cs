﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;


namespace NavHw
{
    public partial class DisneyPage : ContentPage
    {
        public DisneyPage(string addressOfComingsoon)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(DisneyPage)}:  ctor");
            InitializeComponent();

            addressLabel.Text = addressOfComingsoon;
        }
    }
}
