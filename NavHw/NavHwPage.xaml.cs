﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;


namespace NavHw
{
    public partial class NavHwPage : ContentPage
    {
        public NavHwPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavHwPage)}:  ctor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        void OnNickelodeon(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNickelodeon)}");
            Navigation.PushAsync(new NickelodeonPage());
        }

        async void OnDirections(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDirections)}");

            string response = await DisplayActionSheet("Overloaded Action Sheet",
                              "Cancel",
                              null,
                              "Nickelodeon",
                              "CartoonNetwork",
                              "Disney (coming soon)");
            Debug.WriteLine($"User picked:  {response}");

            if (response.Equals("Nickelodeon", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new NickelodeonPage());
            }
            if (response.Equals("CartoonNetwork", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new CartoonNetworkPage());
            }
            if (response.Equals("Disney (coming soon)", StringComparison.OrdinalIgnoreCase))
            {
                await Navigation.PushAsync(new DisneyPage("Coming soon ..."));
            }
        }

        async void OnCartoonNetwork(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCartoonNetwork)}");

            bool usersResponse = await DisplayAlert("This is the CartoonNetwok channel!",
                         "Are you sure you want to be on this channel?",
                         "Yes!",
                         "No");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new CartoonNetworkPage());
            }

        }
    }
}
